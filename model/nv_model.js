// khuôn


function NhanVien(_tk, _ten, _email, _matKhau, _ngayLam, _luongCB, _chucVu, _gioLam) {
    this.tk = _tk,
        this.ten = _ten,
        this.email = _email,
        this.matKhau = _matKhau,
        this.ngayLam = _ngayLam,
        this.luongCB = _luongCB,
        this.chucVu = _chucVu,
        this.gioLam = _gioLam

    this.tongLuong = function () {
        var result = 0;
        if (this.chucVu == 'Sếp') {
            result = this.luongCB * 3
        }
        else if (this.chucVu == 'Trưởng phòng') {
            result = this.luongCB * 2
        }
        else if (this.chucVu == 'Nhân viên') {
            result = this.luongCB * 1
        }
        else {
            return;
        }
        result = new Intl.NumberFormat().format(result);
        return `${result}VND`

    }
    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return 'nhân viên xuất sắc'
        }
        else if (this.gioLam >= 176) {
            return 'nhân viên giỏi'
        }
        else if (this.gioLam >= 160) {
            return 'nhân viên khá'
        }
        else {
            return 'nhân viên trung bình'
        }
    }
}