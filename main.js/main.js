var dsnv = [];

var dataJon = localStorage.getItem("DSNV")
if (dataJon) {
    var dataRaw = JSON.parse(dataJon);
    dsnv = dataRaw.map(function (nv) {
        return new NhanVien(
            nv.tk,
            nv.ten,
            nv.email,
            nv.matKhau,
            nv.ngayLam,
            nv.luongCB,
            nv.chucVu,
            nv.gioLam,
            nv.xepLoai = function () {
                if (this.gioLam >= 192) {
                    return 'nhân viên xuất sắc'
                }
                else if (this.gioLam >= 176) {
                    return 'nhân viên giỏi'
                }
                else if (this.gioLam >= 160) {
                    return 'nhân viên khá'
                }
                else {
                    return 'nhân viên trung bình'
                }
            }

        )
    })
    randerDSNV(dsnv);
}



function saveLocalStorage() {
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJson)
}

function themNguoiDung() {
    // C1:

    // debugger;
    // const idErrors = {
    //     tk: 'tbTKNV',
    //     ten: 'tbTen',
    //     email: 'tbEmail',
    //     matKhau: 'tbMatKhau',
    //     ngayLam: 'tbNgay',
    //     luongCB: 'tbLuongCB',
    //     chucVu: 'tbChucVu',
    //     gioLam: 'tbGiolam'
    // }
    // var nv = layThongTinTuform();
    // var keysForm = Object.keys(idErrors) // [tk, ten]
    // var errors = [] // ["true", "false"]
    // let valid
    // for (let index = 0; index < keysForm.length; index++) {
    //     const field = keysForm[index];
    //     valid = kiemTraRong(nv[`${field}`], idErrors[`${field}`])
    //     errors.push(valid.toString())
    //     // check them email valid
    //     if (field === 'email') {
    //         valid = kiemTraEmail(nv[`email`], idErrors['email'])
    //         errors.push(valid.toString())
    //     }
    // }

    // if (!errors.includes('false')) {
    //     dsnv.push(nv);
    //     saveLocalStorage()
    //     randerDSNV(dsnv);
    //     resetfrom()
    // }
    //C2:
    var nv = layThongTinTuform();
    var isValid = true;
    isValid = isValid & kiemTraRong(nv.tk, "tbTKNV") && checkLength(nv.tk, "tbTKNV") && kiemTraTKNV(nv.tk, dsnv, "tbTKNV")
    isValid = isValid & kiemTraRong(nv.ten, "tbTen") && kiemTraTen(nv.ten, "tbTen")
    isValid &= kiemTraRong(nv.email, "tbEmail") && kiemTraEmail(nv.email, "tbEmail")
    isValid &= kiemTraRong(nv.ngayLam, "tbNgay") && kiemTraNgayThangNam(nv.ngayLam, "tbNgay")
    isValid &= kiemTraRong(nv.luongCB, "tbLuongCB") && kiemTraLuong(nv.luongCB, "tbLuongCB")
    isValid &= kiemTraRong(nv.chucVu, "tbChucVu") && kiemTraChucVu(nv.chucVu, "tbChucVu")
    isValid &= kiemTraRong(nv.gioLam, "tbGiolam") && kiemTraGioLam(nv.gioLam, "tbGiolam")
    isValid &= kiemTraRong(nv.matKhau, "tbMatKhau") && kiemTraPasswd(nv.matKhau, "tbMatKhau")


    if (isValid) {
        dsnv.push(nv);
        saveLocalStorage()
        randerDSNV(dsnv);
        resetfrom()
    }

}
function xoaNv(tkNv) {
    var index = dsnv.findIndex(function (nv) {
        return nv.tk == tkNv;
    });
    console.log(index)
    if (index == -1) {
        return;
    }
    dsnv.splice(index, 1)
    console.log(dsnv.length)
    randerDSNV(dsnv);
    saveLocalStorage();
}

function suaNv(tkNv) {
    var index = dsnv.findIndex(function (nv) {
        return nv.tk == tkNv;
    });
    if (index == -1) return;
    var nv = dsnv[index];
    showThongTinLenForm(nv)
    document.getElementById("tknv").disabled = true;
}
function capNhapNV() {
    var nvEdit = layThongTinTuform();
    console.log('nvEdit', nvEdit)
    var index = dsnv.findIndex(function (nv) {
        return nv.tk == nvEdit.tk;
    });
    if (index === -1) return;
    dsnv[index] = nvEdit;
    saveLocalStorage();
    randerDSNV(dsnv);
    resetfrom();
    document.getElementById("tknv").disabled = false;
}
function resetfrom() {
    document.getElementById("reset").reset();
}
function search() {
    var valueSearch = document.getElementById("searchName").value;
    var search = dsnv.filter(function (value) {
        return value.xepLoai().toUpperCase().includes(valueSearch.toUpperCase())
    })
    randerDSNV(search)
}



